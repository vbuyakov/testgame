      // Generate matrix Array
        function matrixArray(rows,columns,defaultvalue) 
        {
                var arr = [];
                for(var i=0; i<columns; i++)
                {
                    arr[i] = new Array();
                    for(var j=0; j<rows; j++)
                    {
                    arr[i][j] = defaultvalue;
                    }
                }
         return arr;
        }
        //  Game Core  Class      
        function CoinsBoard () {
				this.Scores = [0,0]; // Score for Player 1 and 2;
                this.Coins = matrixArray(BoardSizeC,BoardSizeR,0); // Board array 
                this.GameStarted = null; // The time of the begining of game
                //Init new game
                this.newGame =  function (){this.Coins = matrixArray(BoardSizeC,BoardSizeR,0);}
				//  Reset the Game score
				this.resetScore = function () {this.Scores = [0,0];}
				// Add coin to Matrix
				this.addCoin = function (col ,  row, player)
				{
				this.Coins [row][col] = player;
				}
				// Chaeck for fre places in column
                this.checkFreeSpace = function (col)
                {
				var r = '';
                    for(var row = 0 ; row < BoardSizeR ; row++)
					{
					if(this.Coins[row][col] != 0 ) return  row ;
					}
					
					return  BoardSizeR;
                }
				// Check Columns for win combination 
				this.checkColsForWin= function ()
				{
					var sqstart = 0; // Start position of sequence
					var sqsize = 0; // Size of sequence
					var sqplayer = 0; // Player number in sequence 
					for(var col = 0; col < BoardSizeC; col++)
					{
						for(var row = 0 ;  row < BoardSizeR ; row++)
						{
						if(this.Coins[row][col] != sqplayer )
						{
						sqstart = row;
						sqsize = 1;
						sqplayer = this.Coins[row][col];
						} else if(this.Coins[row][col] != 0 ){ sqsize++;
					
							if(sqsize == 4)
								{ 
								this.Scores[sqplayer-1]++;
								return {'place': 'col',  'pos' : col , 'player': sqplayer , 'start' : sqstart}
								}
						}
						}
					}
					return false;
				}
				
				// Check Rows for win combination 
				this.checkRowsForWin= function ()
				{
					var sqstart = 0; // Start position of sequence
					var sqsize = 0; // Size of sequence
					var sqplayer = 0; // Player number in sequence 
					for(var row = 0 ;  row < BoardSizeR ; row++)
					{
						for(var col = 0; col < BoardSizeC; col++)
						{
						if(this.Coins[row][col] != sqplayer )
						{
						sqstart = col;
						sqsize = 1;
						sqplayer = this.Coins[row][col];
						} else if(this.Coins[row][col] != 0 ){ sqsize++;
					
							if(sqsize == 4)
								{ 
								this.Scores[sqplayer-1]++;
								return {'place': 'row',  'pos' : row , 'player': sqplayer , 'start' : sqstart}
								}
						}
						}
					}
					return false;
				}
            }
        //

//  Init for a Game
Game = new CoinsBoard();
        Game.newGame();

// redraw scrore
function drawScore() 
{
$('#Player1Scrore').html(Game.Scores[0]);
$('#Player2Scrore').html(Game.Scores[1]);
}
// newgame
function newGame()
{
Game.newGame();
for(var row = 0 ;  row < BoardSizeR ; row++)
					{
					
						for(var col = 0; col < BoardSizeC; col++)
						{
						$('#cell_'+col+'_'+row).css('background-color','#5b5bff');
						$('#circle_'+col+'_'+row).css('fill','black');
						}
					}	
GameOver =  false;					
}
// exit the Game
function exitGame()
{
if(confirm("Are you shure you want exit the game?"))
{
	parent.location.href= "http://www.intellias.com/";
}
}

/// Init on Load
	$(function() {
		//Draw the board 
		var cont = '';
		
		for(var col = 0 ;  col < BoardSizeC ; col++)
		{
		cont += '<div class="bhead" id="bhead_'+col+'"></div>';
		}
		
		for(var row = 0 ;  row < BoardSizeR ; row++)
					{
					cont += '<div class="brow">';
						for(var col = 0; col < BoardSizeC; col++)
						{
						cont += '<div class="cell" id="cell_'+col+'_'+row+'" rel="'+col+'"><svg><circle  id="circle_'+col+'_'+row+'" class="coin" cx="2em" cy="2em" r="1.8em"  stroke="#ccc" stroke-opacity=".5" fill="#000" /></svg></div>';
						}
					cont += '</div>';					
					}	
		
		$('#board').html(cont);
		// Init board events
		for(var row = 0 ;  row < BoardSizeR ; row++)
					{
						for(var col = 0; col < BoardSizeC; col++)
						{
						// Cell on Click
						
						$('#cell_'+col+'_'+row).click( function (){
						if(inProc) return; // Check for previos click
						inProc =  true;
						if(GameOver) { inProc = false; return 0; }
						var column = $(this).attr('rel');
						
						var cPos = Game.checkFreeSpace(column) -1;
								if(cPos >= 0)
								{
								Game.addCoin(column, cPos, CurrentPlayer +1 ); // Add coin for a matrix
								$('#circle_'+column+'_'+(cPos)).css('fill',PColors[CurrentPlayer]);
								// Check for winning
								var wr = Game.checkRowsForWin();
								if(wr != false)
									{
									for(var x = wr.start;  x < wr.start + 4 ;  x++)
										{
										
										$('#cell_'+x+'_'+wr.pos).css('background-color',PColors[wr.player -1 ]);
										}
									drawScore();
									alert('Player '+wr.player+' Win!');
									newGame();
									inProc = false;
									return ;
									}
									
								var wr = Game.checkColsForWin();
								if(wr != false)
									{
									for(var y = wr.start;  y < wr.start + 4 ;  y++)
										{
										
										$('#cell_'+wr.pos+'_'+y).css('background-color',PColors[wr.player -1 ]);
										}
									drawScore();	
									alert('Player '+wr.player+' Win!');
									newGame();
									inProc = false;
									return ;
									}	
								
								inProc = false;
								CurrentPlayer = CurrentPlayer == 0 ? 1: 0;
								}
							inProc = false;	
								
								
						});
						// Cell  Mouse Over
						$('#cell_'+col+'_'+row).mouseover(function (){
						if(GameOver) return;
						$('#bhead_'+$(this).attr('rel')).css('background-color', PColors[CurrentPlayer]);
						});
						// Cell Mouse Out
						$('#cell_'+col+'_'+row).mouseout(function (){
						if(GameOver) return;
						$('#bhead_'+$(this).attr('rel')).css('background-color','#5b5bff');
						});
						}
					}	

		//
		$('#exitbutton').click(function () {
			exitGame();
		});
		
		//
		$('#resetscore').click(function () {
				Game.resetScore();
				drawScore();
		});
		
		$('#newgame').click(function () {
				newGame();
		});
		
		
		});
		
		
		


		
		
		
	
		
        